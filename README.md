# A simple Python webapp in a docker container

## Ubuntu

### OS version
`$ lsb_release -a`  

```
No LSB modules are available.  
Distributor ID:	Ubuntu  
Description:	Ubuntu 18.04.2 LTS  
Release:	18.04  
Codename:	bionic  
```

### Python version
The default version of Python installed on Ubuntu is the following.  
`$ python --version`  
```
Python 2.7.15+
```

### Shell name
`$ echo $SHELL`  
```
/bin/bash
```

## Visual Code

### VS version
```
Version: 1.36.1  
Commit: 2213894ea0415ee8c85c5eea0d0ff81ecc191529  
Date: 2019-07-08T22:55:08.091Z  
Electron: 4.2.5  
Chrome: 69.0.3497.128  
Node.js: 10.11.0  
V8: 6.9.427.31-electron.0  
OS: Linux x64 4.15.0-55-generic  
```

### Python extension version
```
Microsoft Python: 2019.6.24221 (9 July 2019)  
```

## Pyenv
Simple Python Version Management

### Installing
The pyenv tool for python is like the nvm tool for node.  
It allows to install multiple python versions and to activate one of them according to your need.  
See also the github project [here](https://github.com/pyenv/pyenv).  
1. Checkout pyenv  
`$ git clone https://github.com/pyenv/pyenv.git ~/.pyenv`  
```
Cloning into '/home/entwicklerfr/.pyenv'  
remote: Enumerating objects: 17352, done.  
remote: Total 17352 (delta 0), reused 0 (delta 0), pack-reused 17352  
Receiving objects: 100% (17352/17352), 3.37 MiB | 4.79 MiB/s, done.  
Resolving deltas: 100% (11818/11818), done.  
```
2.  Define environment variable PYENV_ROOT
`$ echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc`  
`$ echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc`  
3. Add pyenv init to the shell  
`$ echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bashrc`  
4. Restart the shell  
`$ exec "$SHELL"`  
5. Install Python build dependencies before attempting to install a new Python version  
`$ sudo apt-get update; sudo apt-get install --no-install-recommends make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev`  

### Using pyenv
1. Listing the python versions installed on the system  
`$ pyenv versions`  
```
* system  
```

2. Installing new versions  
`$ pyenv install 3.7.4`  
```
Downloading Python-3.7.4.tar.xz...  
-> https://www.python.org/ftp/python/3.7.4/Python-3.7.4.tar.xz  
Installing Python-3.7.4...  
Installed Python-3.7.4 to /home/entwicklerfr/.pyenv/versions/3.7.4  
```
3. Listing installed versions  
`$ pyenv versions`  
```
* system  
  3.7.4  
```
4. Global activation of a Python version  
`$ pyenv versions`  
```
 * system (set by /home/entwicklerfr/.pyenv/version)  
  3.7.4
```
`$ pyenv global`  
```
system  
```  
`$ pyenv global 3.7.4`  
`$ pyenv global`  
```
3.7.4  
```
`$ pyenv versions`  
```
  system  
* 3.7.4 (set by /home/entwicklerfr/.pyenv/version)  
```  
`$ python --version`  
```
Python 3.7.4  
```  
`$ pyenv which python`  
```
/home/entwicklerfr/.pyenv/versions/3.7.4/bin/python  
```

5. Switching versions locally  
See also [here](https://anil.io/blog/python/pyenv/using-pyenv-to-install-multiple-python-versions-tox/)  
> You can also switch the python version you want to use locally (per project) by using a .python-version file at the root of your project. When you enter this directory, pyenv will load the python version’s specified in your .python-version.  
* Switches to a working dir  
`cd ~/Sites/some-project/`  

* Show the current version  
`pyenv which python`  
```
/usr/local/bin/python  
```
`python --version`  
```
Python 2.7.15+
```

* Create a local pyenv  
`pyenv local 3.7.4`  
creates a `.python-version` file with content 3.7.4  

* We changed python version for this directory only  
`python --version`  
```
Python 3.7.4  
```

* Globally, we're still using our default `system`  
`cd .. && python --version`  
```
Python 2.7.15+  
```

## Pipenv
Python Development Workflow for Humans.  
A npm (node) like tool for Python.  
See also [here](https://github.com/pypa/pipenv).
> It automatically creates and manages a virtualenv for your projects, as well as adds/removes packages from your Pipfile as you install/uninstall packages. It also generates the ever-important Pipfile.lock, which is used to produce deterministic builds.

### Installation  
`$ pip install pipenv`  
Successfully installed certifi-2019.6.16 pipenv-2018.11.26 virtualenv-16.7.2 virtualenv-clone-0.5.3  

### To upgrade pipenv at anytime
`$ pip install --user --upgrade pipenv`  

Note: For upgrading pip if necessary : ```pip install --upgrade pip```

## webApp

### Python version setup
* In the projetc directory.  
`cd dev/python-webapp-in-docker`  

`dev/python-webapp-in-docker$ python --version`  
```
Python 2.7.15+  
```

`dev/python-webapp-in-docker$ pyenv which python`  
```
/usr/bin/python  
```

* Setting the local python version to 3.7.4 (a .python-version file is created)
`dev/python-webapp-in-docker$ pyenv local 3.7.4`  

`dev/python-webapp-in-docker$ pyenv which python`  
```
/home/entwicklerfr/.pyenv/versions/3.7.4/bin/python  
```

`entwicklerfr@entwicklerfr-pc:~/dev/python-webapp-in-docker$ python --version`  
```
Python 3.7.4  
```

### Project virtual env initialization
This will create your python virtual environment, which you can then use by running pipenv shell.  
The best part is that Pipenv supports Pyenv and will use python versions installed using that tool making our system much cleaner.  
It creates a Pipfile.lock file which looks like a package.json file generated by npm.  
`$ pipenv --python 3.7.4 install`  
![alt text](readme/screenshot-create-virtualenv.png "pipenv install")  

### Installing Flask 
It's a Lightweight WSGI web application framework.  
`$ pipenv install flask`  

### Clone a python project
When you clone a python project, a `pipenv install` will install python packages defined in Pipfile as `npm install` does in a node environment.

### Coding
Inspired by https://www.freecodecamp.org/news/how-to-build-a-web-application-using-flask-and-deploy-it-to-the-cloud-3551c985e492/  

Create the main.py file...pass :-)