from flask import Flask

app = Flask(__name__)

@app.route("/")
def home():
    return "Hello, World!"
    
if __name__ == "__main__":
    # (debug = true) :
    # the server will automatically reload for code changes and show a debugger in case an exception happened
    app.run(debug=True)